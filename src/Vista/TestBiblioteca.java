/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Mensaje;
import Negocio.Biblioteca;
import Util.seed.ListaCD;
import com.itextpdf.text.DocumentException;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DOCENTE
 */
public class TestBiblioteca {

    public static void main(String[] args) {
        //cargamos archivos
        String urlFacultades = "https://gitlab.com/madarme/archivos-persistencia/raw/master/facultad.csv";
        String urlEstudiantes = "https://gitlab.com/madarme/archivos-persistencia/raw/master/estudiantes.csv";
        String urlInventario = "https://gitlab.com/madarme/archivos-persistencia/-/raw/master/inventario.csv?ref_type=heads";
        String urlSolicitudes = "https://gitlab.com/madarme/archivos-persistencia/raw/master/solicitudes.csv";
        //creamos La bibblioteca
        Biblioteca biblio = new Biblioteca();
//        Biblioteca biblio=new Biblioteca(urlSolicitudes, urlFacultades, urlEstudiantes, urlInventario);

        //cargamos
        biblio.cargarFacultades(urlFacultades);
        biblio.cargarEstudiantes(urlEstudiantes);
        biblio.cargarInventario(urlInventario);
        biblio.cargarSolicitudes(urlSolicitudes);

//        //imprmimos la prueba en consola
        System.out.println("\n-------------cargando facultades----------\n"
                + biblio.getFacultades());
        System.out.println("\n-------------cargando estudiantes----------\n"
                + biblio.getEstudiantes());
        System.out.println("\n-------------cargando solicitudes----------\n"
                + biblio.getSolicitudes());
//        System.out.println("\n-------------cargando inventario----------\n"
//                +biblio.getSolicitudes());

        ListaCD<Mensaje> r = biblio.procesarSolicitudes();
        System.out.println(r.toString());
        try {
            biblio.crearPDF(r);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestBiblioteca.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(TestBiblioteca.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
