/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template

Enunciado:
https://docs.google.com/document/d/1cAXPd_GRT8PyyP85mc52qqf2gRnbILl9/edit?usp=sharing&ouid=107614452311706692726&rtpof=true&sd=true
 */
package Negocio;

import Modelo.Estudiante;
import Modelo.Facultad;
import Modelo.Libro;
import Modelo.Mensaje;
import Modelo.Solicitud;
import Util.seed.ArchivoLeerURL;
import Util.seed.ListaCD;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 *
 * @author DOCENTE
 */
public class Biblioteca {

    private ListaCD<Facultad> facultades = new ListaCD();
    private ListaCD<Solicitud> solicitudes = new ListaCD();
    private ListaCD<Estudiante> estudiantes = new ListaCD();

    public Biblioteca() {
        
    }

    public Biblioteca(String urlSolicitudes, String urlFactultades, String urlEstudiantes, String urlInventario) {
        cargarFacultades(urlFactultades);
        cargarEstudiantes(urlEstudiantes);
        cargarSolicitudes(urlSolicitudes);
        cargarInventario(urlInventario);
    }
    public String getFacultades() {
        String msg ="";
        for(Facultad dato : this.facultades){
        msg += dato.toString() + "\n";
        }
        return msg;
    }
    public String getSolicitudes() {
        String msg ="";
        for(Solicitud dato : this.solicitudes){
        msg += dato.toString() + "\n";
        }
        return msg;
    }
    public String getEstudiantes() {
        String msg ="";
        for(Estudiante dato : this.estudiantes){
        msg += dato.toString() + "\n";
        }
        return msg;
    }

//    ListaCD<Facultad> getFacultades() {
//        return facultades;
//    }
//    
//    public ListaCD<Solicitud> getSolicitudes() {
//        return solicitudes;
//    }
//
//    public ListaCD<Estudiante> getEstudiantes() {
//        return estudiantes;
//    }

    public void cargarSolicitudes(String urlSolicitudes) {
        ListaCD<Solicitud> sol = new ListaCD<>();
        ArchivoLeerURL archivo = new ArchivoLeerURL(urlSolicitudes);
        Object linea[] = archivo.leerArchivo();
        for (int i = 1; i < linea.length; i++) {
            String fila = linea[i].toString();
            //CODLIBRO;TIPO_SERVICIO;CODIGO ESTUDIANTE
            String datos[] = fila.split(";");
            int codigoLibro = Integer.parseInt(datos[0]);
            Byte tipoServicio = Byte.parseByte(datos[1]);
            int codigoEstudiante = Integer.parseInt(datos[2]);
            Solicitud nuevaSol = new Solicitud(codigoLibro, tipoServicio, codigoEstudiante);
            sol.insertarFinal(nuevaSol);
        }
        this.solicitudes = sol;

    }

    public void cargarFacultades(String urlFactultades) {
        ListaCD<Facultad> fac = new ListaCD();
        ArchivoLeerURL archivo = new ArchivoLeerURL(urlFactultades);
        Object linea[] = archivo.leerArchivo();
        for (int i = 1; i < linea.length; i++) {
            String fila = linea[i].toString();
            //cod;nombreFacultad
            String datos[] = fila.split(";");
            int codigo = Integer.parseInt(datos[0]);
            if (codigo < 1 || codigo > 9) {
                throw new IllegalArgumentException("El código de la Facultad está fuera del rango permitido (1-9): " + codigo);
            }
            String nombreFacultad = datos[1];
            Facultad nuevaFac = new Facultad(codigo, nombreFacultad);
            fac.insertarFinal(nuevaFac);
        }
        this.facultades=fac;
    }

//    public void crearFacultades(String urlFactultades) {
//        ListaCD<Facultad> fac = new ListaCD();
//        ArchivoLeerURL archivo = new ArchivoLeerURL(urlFactultades);
//        Object linea[] = archivo.leerArchivo();
//        for (int i = 1; i < linea.length; i++) {
//            String fila = linea[i].toString();
//            //cod;nombreFacultad
//            String datos[] = fila.split(";");
//            int codigo = Integer.parseInt(datos[0]);
//            String nombreFacultad = datos[1];
//            Facultad nuevaFac = new Facultad(codigo, nombreFacultad);
//            fac.insertarFinal(nuevaFac);
//        }
//        this.facultades = fac;
//    }
    public void cargarEstudiantes(String urlEstudiantes) {
        ListaCD<Estudiante> est = new ListaCD<>();
        ArchivoLeerURL archivo = new ArchivoLeerURL(urlEstudiantes);
        Object linea[] = archivo.leerArchivo();
        for (int i = 1; i < linea.length; i++) {
            String fila = linea[i].toString();
            //codigos;nombre de estudiante;email;semestre
            String[] datos = fila.split(";");
            int codigo = Integer.parseInt(datos[0]);
            String nombreEstudiante = datos[1];
            String correoEstudiante = datos[2];
            Byte semestre = Byte.parseByte(datos[3]);
//            if (codigo < 1 || codigo > 10) {
//                throw new IllegalArgumentException("El semestre no es valido (1-10): " + semestre);
//            }
            Estudiante nuevoEst = new Estudiante(semestre, codigo, nombreEstudiante, correoEstudiante);
            est.insertarFinal(nuevoEst);
        }
        this.estudiantes=est;
    }

    public void cargarInventario(String urlInventario) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(urlInventario);
        Object linea[] = archivo.leerArchivo();
        for (int i = 1; i < linea.length; i++) {
            String fila = linea[i].toString();
            //CODLIBRO;NOMBRE
            String[] datos = fila.split(";");
            int codLibro = Integer.parseInt(datos[0]);
            if (codLibro < 100000 || codLibro > 999999) {
                throw new IllegalArgumentException("El código de libro debe tener 6 cifras: " + codLibro);
            }
            String nombreInventario = datos[1];

            Libro l = new Libro(codLibro, nombreInventario);
            int codigoFacultadDelLibro = l.getNumeroFacultad(codLibro);
            Facultad facultad = buscarFacultadPorCodigo(codigoFacultadDelLibro);
            if(facultad!=null){
                facultad.agregarLibro(l);
//            }else{
//                throw new RuntimeException("facultad inexistente : "+facultad);
            }
        }
    }

    private Facultad buscarFacultadPorCodigo(int codigoABuscar) {
        for (Facultad fcl : this.facultades) {
            if (fcl.getCodigo() == codigoABuscar) {
                return fcl;
            }
        }
        return null;
    }
    
    public Estudiante getEstudiante(int codigo) {

        for (Estudiante x : this.estudiantes) {

            if (x.getCodigo() == codigo) {
                return x;
            }

        }

        return null;
    }

    public Libro getLibroCodigo(int codigo) {

        for (Facultad x : this.facultades) {

            ListaCD<Libro> libros = x.getEstante().getLibros();
            for (Libro temp : libros) {

                if (temp != null && temp.getCodigo() == codigo) {
                    return temp;
                }

            }
        }

        return null;
    }

    public Facultad getFacultadLibro(int codigo) {

        for (Facultad x : this.facultades) {

            ListaCD<Libro> libros = x.getEstante().getLibros();
            for (Libro temp : libros) {

                if (temp != null && temp.getCodigo() == codigo) {
                    return x;
                }
            }
        }

        return null;
    }
    
    public boolean codigoValido(int codigo) {

        if (codigo < 100000 || codigo > 999999) {
            return false;
        }

        return true;
    }
    
    private Mensaje procesarSolicitud(Solicitud solicitud) {
        String descripcion = "";
        int codigoLibro = solicitud.getCodLibro();
        int tipoServicio = solicitud.getTipoServicio();
        int codigoEstudiante = solicitud.getCodEstudiante();

        Estudiante estudiante = getEstudiante(codigoEstudiante);
        Libro libro = getLibroCodigo(codigoLibro);

        if (estudiante == null) {
            return new Mensaje("El código del estudiante: " + codigoEstudiante + " es inválido");
        }

        if (!codigoValido(codigoLibro) == true) {
            return new Mensaje("El código del libro: " + codigoLibro + " es inválido");
        }

        Facultad facultad = getFacultadLibro(codigoLibro);

        if (tipoServicio == 1) {
            if (estudiante != null && libro != null && estudiante.agregarLibro(libro)) {
                facultad.getEstante().reservarLibro(libro);
                descripcion = "Se reservó el libro con código: " + codigoLibro + " al estudiante de código: " + codigoEstudiante;
            } else if (libro == null) {
                descripcion = "No se reservó el libro con código: " + codigoLibro + " no se encontraba en el inventario";
            } else {
                descripcion = "No se reservó el libro con código: " + codigoLibro + " POR QUE el estudiante con código: " + codigoEstudiante + " excedió su cupo";
            }
        } else if (tipoServicio == 2) {
            if (estudiante != null && codigoValido(codigoLibro)) {
                Libro nuevoLibro = new Libro(codigoLibro, "");
                facultades.get(codigoLibro / 100000).getEstante().devoluciónLibro(libro);
                descripcion = "Se entregó el libro con código: " + codigoLibro + " del estudiante de código: " + codigoEstudiante;
            } else {
                descripcion = "No se entregó el libro con código: " + codigoLibro + " debido a una condición inválida";
            }
        }

        return new Mensaje(descripcion);
    }


    public ListaCD<Mensaje> procesarSolicitudes() {
        ListaCD<Mensaje> total = new ListaCD();
        for(Solicitud sol : solicitudes){
            Mensaje msj = procesarSolicitud(sol);
            total.insertarInicio(msj);
        }
        return total;
    }

    public void crearPDF(ListaCD<Mensaje> resultado) throws FileNotFoundException, DocumentException{
        if (resultado.isEmpty()) {
            throw new RuntimeException("Imposible imprimir datos vacíos");
        }
        long num = System.currentTimeMillis();
        //1. Crear el objeto que va a formatear el pdf:
        Document documento = new Document();
        //2. Crear el archivo de almacenamiento--> PDF
        String name = "fichero-" + num + ".pdf";
        FileOutputStream ficheroPdf = new FileOutputStream("src/pdf/" + name);
        //3. Asignar la estructura del pdf al archivo físico:
        PdfWriter.getInstance(documento, ficheroPdf);
        documento.open();
        //Creando parrafos:
        Paragraph parrafo = new Paragraph();
        Chunk chunkNegrita = new Chunk("Transacciones Del Sistema \n");
        chunkNegrita.setFont(FontFactory.getFont(FontFactory.HELVETICA_BOLD));
        parrafo.add(chunkNegrita);
        parrafo.setAlignment(Element.ALIGN_CENTER);
        
        PdfPTable table = new PdfPTable(1);
        boolean sw = true;
        
        for (Mensaje datos : resultado) {
            PdfPCell celda = new PdfPCell(new Phrase(datos.toString()));
            celda.setBorderColor(BaseColor.BLACK);
            celda.setPadding(8);

            if (sw) {
                celda.setBackgroundColor(BaseColor.LIGHT_GRAY);
            }

            table.addCell(celda);
            sw = !sw;
        }
        parrafo.add(table);
        documento.add(parrafo);
        documento.close();
    }

}
