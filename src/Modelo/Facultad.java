/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.seed.ListaCD;

/**
 *
 * @author DOCENTE
 */
public class Facultad {
    
    private int codigo;
    private String nombre;
    private Estante estante = new Estante();

    public Facultad() {
    }

    public Facultad(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Estante getEstante() {
        return estante;
    }

    public void setEstante(Estante estante) {
        this.estante = estante;
    }

    @Override
    public String toString() {
        return "Facultad{" + "codigo=" + codigo + ", nombre=" + nombre + ", estante=" + estante + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Facultad other = (Facultad) obj;
        return this.codigo == other.codigo;
    }

    public void agregarLibro(Libro l){
        this.estante.getLibros().insertarFinal(l);
    }
    
}
