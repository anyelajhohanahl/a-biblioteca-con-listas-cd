/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author DOCENTE
 */
public class Solicitud {
    
    private int codLibro;
    private byte tipoServicio;
    private int codEstudiante;

    public Solicitud() {
    }

    public Solicitud(int codLibro, byte tipoServicio, int codEstudiante) {
        this.codLibro = codLibro;
        this.tipoServicio = tipoServicio;
        this.codEstudiante = codEstudiante;
    }

    public int getCodLibro() {
        return codLibro;
    }

    public void setCodLibro(int codLibro) {
        this.codLibro = codLibro;
    }

    public byte getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(byte tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public int getCodEstudiante() {
        return codEstudiante;
    }

    public void setCodEstudiante(int codEstudiante) {
        this.codEstudiante = codEstudiante;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Solicitud other = (Solicitud) obj;
        if (this.codLibro != other.codLibro) {
            return false;
        }
        if (this.tipoServicio != other.tipoServicio) {
            return false;
        }
        return this.codEstudiante == other.codEstudiante;
    }

    @Override
    public String toString() {
        return "Solicitud{" + "codLibro=" + codLibro + ", tipoServicio=" + tipoServicio + ", codEstudiante=" + codEstudiante + '}';
    }
    
}
