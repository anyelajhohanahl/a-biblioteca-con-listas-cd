/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.seed.ListaCD;
import java.util.Objects;

/**
 *
 * @author DOCENTE
 */
public class Estante {

    private ListaCD<Libro> libros = new ListaCD();

    public Estante() {
    }

    public ListaCD<Libro> getLibros() {
        return libros;
    }

    public void setLibros(ListaCD<Libro> libros) {
        this.libros = libros;
    }

    @Override
    public String toString() {

//        return "Estante{" + "libros=" + libros + '}';
        String msg = "";
        for (Libro libro : this.libros) {
            msg += libro.toString() + "\n";
        }
        return msg;

    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estante other = (Estante) obj;
        return Objects.equals(this.libros, other.libros);
    }

    public void reservarLibro(Libro l) {
        this.libros.removerElemento(l);
    }

    public void devoluciónLibro(Libro l) {
        this.libros.insertarFinal(l);
    }

}
