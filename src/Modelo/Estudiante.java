/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author DOCENTE
 */
public class Estudiante {
    
    private byte semestre;
    private int codigo;
    private String nombre;
    private String email;
    private Libro librosPrestados[]= {null ,null};

    public Estudiante() {
    }

    public Estudiante(byte semestre, int codigo, String nombre, String email) {
        this.semestre = semestre;
        this.codigo = codigo;
        this.nombre = nombre;
        this.email = email;
    }
    
    public boolean agregarLibro(Libro l){
        if(this.librosPrestados[0]==null){
           this.librosPrestados[0]=l;
           return true;
        }
        else if(this.librosPrestados[1]==null){
           this.librosPrestados[1]=l;
           return true;
        }
        return false;
    }

    public byte getSemestre() {
        return semestre;
    }

    public void setSemestre(byte semestre) {
        this.semestre = semestre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Estudiante{" + " codigo=" + codigo + ", nombre=" + nombre + ", email=" + email + "semestre=" + semestre + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        return this.codigo == other.codigo;
    }
    
    
    
}
